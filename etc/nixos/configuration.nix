# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

#let
#  # bash script to let dbus know about important env variables and
#  # propagate them to relevent services run at the end of sway config
#  # see
#  # https://github.com/emersion/xdg-desktop-portal-wlr/wiki/"It-doesn't-work"-Troubleshooting-Checklist
#  # note: this is pretty much the same as  /etc/sway/config.d/nixos.conf but also restarts
#  # some user services to make sure they have the correct environment variables
#  dbus-sway-environment = pkgs.writeTextFile {
#    name = "dbus-sway-environment";
#    destination = "/bin/dbus-sway-environment";
#    executable = true;
#    text = ''
#  dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
#  systemctl --user stop pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
#  systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
#      '';
#  };
#  # currently, there is some friction between sway and gtk:
#  # https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
#  # the suggested way to set gtk settings is with gsettings
#  # for gsettings to work, we need to tell it where the schemas are
#  # using the XDG_DATA_DIR environment variable
#  # run at the end of sway config
#  configure-gtk = pkgs.writeTextFile {
#      name = "configure-gtk";
#      destination = "/bin/configure-gtk";
#      executable = true;
#      text = let
#        schema = pkgs.gsettings-desktop-schemas;
#        datadir = "${schema}/share/gsettings-schemas/${schema.name}";
#      in ''
#        export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
#        gnome_schema=org.gnome.desktop.interface
#        gsettings set $gnome_schema gtk-theme 'Dracula'
#        '';
#  };
#
#in
{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    #./printers.nix
  ];

  # Bootloader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      efi.efiSysMountPoint = "/boot/efi";
    };
    plymouth = {
      enable = true;
    };
    extraModulePackages = with config.boot.kernelPackages; [
      v4l2loopback
    ];
    #extraModprobeConfig = ''
    #  options v4l2loopback devices=1 video_nr=1 card_label="OBS Cam" exclusive_caps=1
    #'';
  };

  
  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  # Enable swap on luks
  boot.initrd.luks.devices."luks-ffc90af6-9001-4504-a518-069086690c0b".device = "/dev/disk/by-uuid/ffc90af6-9001-4504-a518-069086690c0b";
  boot.initrd.luks.devices."luks-ffc90af6-9001-4504-a518-069086690c0b".keyFile = "/crypto_keyfile.bin";

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;
  systemd.services.NetworkManager-wait-online.enable = false;

  # Set your time zone.
  time.timeZone = "Europe/Copenhagen";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_DK.utf8";
  i18n.extraLocaleSettings = {
    LC_ALL = "en_DK.utf8";
  };

  services.greetd = {
    enable = true;
    settings = rec {
      initial_session = {
        command = "${pkgs.bash}/bin/bash -l ${pkgs.sway}/bin/sway 2> /home/alex/.config/sway/err.log";
        user="alex";
      };
      default_session = initial_session;
    };
  };
  # WM (Window Manager) / DE (Desktop Environment)
#  services.xserver = {
#    # Enable the X11 windowing system.
#    enable = true;
#    # Enable the KDE Plasma Desktop Environment.
#    desktopManager.plasma5.enable = false;
#    # Enable automatic login for the user (Disable if you are NOT using disk encryption)
#    displayManager = {
#      sddm.enable = false;
#      sddm.settings = {
#        Autologin = {
#          User = "alex";
#          #Session = "bash -l sway > /home/alex/.config/sway/sway.log";
#          Session = "sway";
#        };
#      };
#      #autoLogin.enable = true;
#      #autoLogin.user = "alex";
#      #defaultSession = "sway";
#    };
    # Configure keymap in X11
#    layout = "us";
#    xkbVariant = "altgr-intl";
#    # Enable touchpad support (enabled default in most desktopManager).
#    # libinput.enable = true;
#  };
#

  ## SWAY
  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
    # kbd keyboard config done in .local/sway/config
  };
  programs.light.enable = true;
  services.gnome.gnome-keyring.enable = true;


  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security = {
    rtkit.enable = true;
    polkit.enable = true;
  };
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };
  # Enable bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true; # provides blueman-applet and blueman-manager using bluetoothctl

  # Allow specific unfree packages (cannot be defined at user level in this file?)
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "steam"
    "steam-original"
    "steam-runtime"
    "steam-run"
    "iscan"
    "iscan-gt-f720-bundle"
    "iscan-gt-1500-bundle"
  ];
  
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.alex = {
    isNormalUser = true;
    description = "Alex Loss";
    extraGroups = [ 
      "networkmanager" # ability to edit network connections (e.g. wifi)
      "wheel" # Ability to become super user
      "dialout" # Allows access to serial ports.
      "video" # .. ?
      "libvirtd" # Use the virtualisation capabilities (e.g. libvirt)
      "qemu-libvirtd" # Use the virtualisation capabilities (e.g. libvirt)
      "docker"
    ];
    packages = with pkgs; [
      # Games
      lutris
      steam
      wine
      winetricks
      protontricks # winetricks helper for Steam

      # DEV
      ## IDE
      vscodium
      emacs
      helix
      kate
      lapce

      ## Python
      python310 # used python3 -m ensurepip to install pip and everything else there
      (
        python310.withPackages(ps: with ps; [
          setuptools
          #pandas
          #ipykernel
          matplotlib
          #opencv
          paho-mqtt
          protobuf
          pylint # A bug and style checker for Python
          babel #translation stuff
          jinja2 # templating
          # LSP (Language Server Protocol)
          #pyls-black
          pyls-flake8
          pyls-isort
          pylsp-mypy
          # Pytest
          #pytest
          #pytest-pylint # too many tests before compiling
          # Documentation
          #sphinx
          #sphinx-argparse # makes sense of argparse # install bug
          #sphinx-autobuild # auto rebuild docs
          #sphinx-serve # Spawns a simple HTTP server to preview your sphinx documents
          
          #kikit # KiCAD multitool backend, allows to panelise boards together.
        ])
      )

      #python310Packages.pip
      #python310Packages.tox
      ## Web
      hugo
      nodejs
      ## Embedded
      arduino
      gcc-arm-embedded
      pkgsCross.avr.buildPackages.gcc # avr-gcc, only qmk for now
      avrdude
      dfu-programmer
      dfu-util
      ## Virtualisaton / Containers
      docker
      docker-compose
      libvirt
      podman # A program for managing pods, containers and container images
      podman-desktop # A graphical tool for developing on containers and Kubernetes
      podman-tui # Podman Terminal UI
      podman-compose # An implementation of docker-compose with podman backend
      pods # A podman desktop application
      qemu
      ## Databases
      dbeaver # Database GUI tool (foss)
      ## Misc
      rustc
      cargo
      git
      delta # syntax-highlight git diff
      gcc
      go
      ## Dbg
      rr
      gdb

      # Browsers
      firefox
      lynx # text based browser
      chromium

      # CAD
      #blender # 3D 
      freecad # 3D CAD
      #kicad # Electronic PCBs
      inkscape # 2D vector graphic SVGs

      # Creative
      gimp # Raster image editor
      krita # Painting program

      # Office
      libreoffice # Workhorse office suite
      scribus # Desktop Publishing (DTP) and Layout program (like inDesign)
      pandoc # convert between file formats
      texlive.combined.scheme-full # LaTeX engine used for pandoc
      groff # convert groff markdown to pdf / html
      ghostwriter # Minimal-ish markdown editor
      zathura # minimal PDF viewer
      okular # gui PDF viewer (Use LibreOffice Draw for editing PDFs
      latexrun
      tetex
      texlab

      # Communication / Chat
      signal-desktop # Signal chat
      thunderbird # Mail

      # Multimedia
      mpv

      # DE / WM

      # Misc
      cura # 3D printing slicer
      prusa-slicer # 3D printing slicer
      nmap # network scanner / mapper; Do NOT use outside your own network
      transmission-qt
      neofetch # Display system info
      gparted # Gui partition manager
      birdfont # Font editor which can generate fonts in TTF, EOT, SVG and BIRDFONT format
      fontpreview
      libstdcxx5
      cups # A standards-based printing system for UNIX
      tldr # Examples for usage of a command
      tokei # A program that allows you to count your code, quickly (stats)
      micro # better nano
      rpi-imager # Download and Flash RPi images and derivatives.
      #via
      vial
      whois # Get DNS info about a url
    ];
  };

  # Home manager
  #home-manager.users.alex = {
  #  programs.zsh.enable = true;
  #};
  users.users.podman = {
    isNormalUser = true;
    description = "Podman user";
    extraGroups = [ 
      "networkmanager" # ability to edit network connections (e.g. wifi)
    ];
    packages = with pkgs; [
      podman # A program for managing pods, containers and container images
      podman-compose # An implementation of docker-compose with podman backend
      pods # A podman desktop application
    ];
  };
  #nixpkgs.config.packageOverrides = pkgs: { tox = pkgs.tox-venv; };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    
    home-manager # Allows the user to use nix package manager and install stuff locally to them

    neovim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.

    (neovim.override {  
       viAlias = true;
       vimAlias = true;
       configure = {
         packages.myplugins = with pkgs.vimPlugins; {
           start = [ vim-nix vim-lastplace statix ];
           opt = [];
         };
      };
    })
    # TERMIMAL
    alacritty

    # TOOLS
    ranger # file manager
    bat # cat, but better (rust)
    #bat-extras.batdiff # doesn't add anything to diff...
    bat-extras.batgrep
    bat-extras.batman
    bat-extras.batwatch
    bat-extras.prettybat
    fd # A simple, fast and user-friendly alternative to find
    sd # Intuitive find & replace CLI (sed alternative)
    wget
    gnumake
    #ripgrep-all
    (ripgrep-all.overrideAttrs (old: { 
      # compile failure
      # https://github.com/NixOS/nixpkgs/issues/250306
      doInstallCheck = false; 
    }))
    lsd
    ncdu # Detect which files take up space
    htop
    bottom # htop alternative
    jq
    ijq # interactive jq
    yq # cli yaml manipulation tool
    ffmpeg # media processing
    entr # do X when these files are updated
    # Archive tools
    dtrx # cli archive tool
    ark # gui archive tool (kde)
    p7zip
    zip
    unzip
    unar # adds support to extract *.rar
    usbutils
    util-linux

    # File systems
    exfat
    ntfs3g
    fuse3
    fuse-overlayfs
    # VPN
    openvpn

    # WM / DE
    ## Sway
    sway
    sway-contrib.inactive-windows-transparency
    sway-launcher-desktop
    swaybg
    swaycwd
    swayidle
    swaylock
    swaywsr
    waybar
    i3blocks
    sysstat # Required by i3blocks cpu stats
    acpi # Read Battery and other stuff
    wdisplays # configuring displays in Wayland compositors

    ## various bits and bobs
    #configure-gtk
    wayland
    glib # gsettings
    dracula-theme # gtk theme
    gnome3.adwaita-icon-theme  # default gnome cursors
    grim # screenshot functionality
    slurp # screenshot functionality
    wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
    bemenu # wayland clone of dmenu
    mako # notification system developed by swaywm maintainer
    rofi-wayland # application selector/menu/sherlock
    libnotify # Send notifications to notification manager
    graphviz # Graph visualization tools

    # media
    mpv # general purpose media player
    imv # img viewer
    #(pkgs.wrapOBS { # Open Broadcast Software
    #  plugins = with pkgs.obs-studio-plugins; [
    #    wlrobs
    #    obs-backgroundremoval
    #    obs-pipewire-audio-capture
    #  ];
    #})

    # serial debugging
    minicom
    screen

    # Virtualisation
    #virt-manager # Gui tool for using QEMU / KVM
    #qemu_kvm # A generic and open source machine emulator and virtualizer (cpu only)
    #OVMFFull # Sample UEFI firmware for QEMU and KVM
    #swtpm #-tpm2 # Libtpms-based TPM emulator
    #swtpm-tpm2 # Libtpms-based TPM emulator
    gnu-efi # GNU EFI development toolchain
    edk2 # Intel EFI development kit
    win-virtio # Windows VirtIO Drivers
    quickemu # Quickly create and run optimised Windows, macOS and Linux desktop virtual machines

  ];


  ## VIRTUALISATION
  #virtualisation.kvmgt.enable = true;
  virtualisation.docker.enable = true;
  virtualisation.libvirtd = {
    enable = true;

    #onShutdown = "suspend";
    #onBoot = "ignore";


    qemu = {
      package = pkgs.qemu_kvm;
      ovmf.enable = true;
      #ovmf.package = pkgs.OVMFFull;
      swtpm.enable = true;
      swtpm.package = pkgs.swtpm; #-tpm2;
      runAsRoot = false;
    };
  };
  programs.dconf.enable = true;
  environment = {
    etc = {
      "ovmf/edk2-x86_64-secure-code.fd" = {
        source = config.virtualisation.libvirtd.qemu.package + "/share/qemu/edk2-x86_64-secure-code.fd";
      };

      "ovmf/edk2-i386-vars.fd" = {
        source = config.virtualisation.libvirtd.qemu.package + "/share/qemu/edk2-i386-vars.fd";
        mode = "0644";
        user = "libvirtd";
      };
    };
    variables = { 
      # Default programs
      EDITOR = "vim";
      BROWSER = "firefox";
      # Virtualisation
      ENV_EFI_CODE_SECURE="/run/libvirt/nix-ovmf/OVMF_CODE.fd";
      ENV_EFI_VARS_SECURE="/run/libvirt/nix-ovmf/OVMF_VARS.fd";
    };
    interactiveShellInit = ''
      alias gs='git status'
    '';
      #alias unzip='7z x'
  };

  
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  ## STEAM
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = false; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = false; # Open ports in the firewall for Source Dedicated Server
  };

  fonts = {
    packages = with pkgs; [
      cantarell-fonts
      cascadia-code # Monospaced font that includes programming ligatures
      #corefonts # Microsoft's TrueType core fonts for the Web (unfree)
      dejavu_fonts
      doulos-sil # International Phonetic Alphabet
      emacs-all-the-icons-fonts
      font-awesome
      liberation_ttf
      noto-fonts
      # noto-fonts-cjk # Chinese, Japanese, Korean
      noto-fonts-emoji
      ubuntu_font_family
      noto-fonts-extra
      roboto # android

      (nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })
    ];
    enableDefaultPackages = true;
    fontconfig = {
      defaultFonts = {
        serif = [ "Ubuntu" ];
        sansSerif = [ "Ubuntu" ];
        monospace = [ "FiraCode" ];
      };
    };
    enableGhostscriptFonts = true;

    fontDir.enable = true; # cd /nix/var/nix/profiles/system/sw/share/X11/fonts
  };

  services.tailscale.enable = true;

  # xdg-desktop-portal works by exposing a series of D-Bus interfaces
  # known as portals under a well-known name
  # (org.freedesktop.portal.Desktop) and object path
  # (/org/freedesktop/portal/desktop).
  # The portal interfaces include APIs for file access, opening URIs,
  # printing and others.
  services.dbus.enable = true;
  xdg.portal = {
    enable = true;
    wlr.enable = true;
    # gtk portal needed to make gtk apps happy
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };
  services.avahi.enable = true;
  
  nix = {
    settings.auto-optimise-store = true;
    gc = {
      # Garbage Collection (gc). Automatically delete old nix generations
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
  };

# This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}

